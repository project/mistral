# Drupal LLM Provider for Mistral API

## Overview

This module serves as an LLM Provider for  Mistral

## Usage

To use the Drupal LLM Provider for the LM Studio API module,
enable the module as you would  with any other Drupal module.

Dependency of this module is the
[LLM Provider Service](https://www.drupal.org/project/llm_provider) module,
which contains the  LLM Chat Example submodule - enable it to chat
with your LLM from Drupal.

## Requirements

- Drupal 9 or 10
- [LLM Provider Service](https://www.drupal.org/project/llm_provider) module.

## Installation

1. Install as you would normally install a contributed Drupal module. Visit:
   [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules)
   for further information.
2. Enable the module through the Drupal administration interface or via Drush
   (`drush en mistral`).

## Support & Maintenance

For any issues or feature requests, please use the
[issue tracker](https://www.drupal.org/project/issues/mistral) on
the project page.

## Maintainer

