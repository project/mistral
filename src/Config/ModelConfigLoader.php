<?php

namespace Drupal\mistral\Config;

use Drupal\llm_provider\Enum\Bundles;
use Drupal\llm_provider\Utility\StringUtility;

/**
 * Loader of LLM settings.
 */
class ModelConfigLoader {

  /**
   * Returns LLM-specific settings.
   *
   * @return array[]
   *   Configurations.
   */
  public static function loadSettings(): array {
    return [
      StringUtility::pascalToSnake(Bundles::Chat->name) => [
        'endpoint' => '/v1/chat/completions',
        'llms' => [
          'deterministic_chat' => [
            'title' => t('Deterministic Chat'),
            'settings' => [
              'temperature' => 0.0,
            ],
          ],
          'high_temperature_chat' => [
            'title' => t('High Temperature Chat'),
            'settings' => [
              'temperature' => 1.0,
              'top_k' => 200,
            ],
          ],
        ],
      ],
      StringUtility::pascalToSnake(Bundles::Embedding->name) => [
        'endpoint' => '/v1/embeddings',
        'llms' => [
          'text_embedding' => [
            'title' => t('Embeddings'),
          ],
        ],
      ],
    ];
  }

}
