<?php

namespace Drupal\mistral\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form to set the API Url and port Mistral.
 */
class MistralSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['mistral.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mistral_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('mistral.settings');

    $form['mistral_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mistral Domain/IP'),
      '#default_value' => (!empty($config->get('domain')) ? $config->get('domain') : "https://api.mistral.ai/v1"),
      '#description' => $this->t('Normally this is https://api.mistral.ai/v1'),
      '#required' => TRUE,
    ];

    $form['mistral_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mistral key'),
      '#default_value' => $config->get('api_key'),
      '#description' => $this->t('Provide api key. Find your keys here: https://console.mistral.ai/api-keys/.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('mistral.settings')
      ->set('domain', $form_state->getValue('mistral_domain'))
      ->set('api_key', $form_state->getValue('mistral_key'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
