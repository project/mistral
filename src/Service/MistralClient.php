<?php

namespace Drupal\mistral\Service;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\llm_provider\Base\LLMProviderClientBase;
use Drupal\llm_provider\Enum\Bundles;
use Drupal\llm_provider\Utility\StringUtility;
use Drupal\mistral\Config\ModelConfigLoader;

/**
 * Service to handle API requests to the LM Studio local server.
 */
class MistralClient extends LLMProviderClientBase {

  /**
   * {@inheritdoc}
   */
  public function getConfig(): ImmutableConfig {
    return $this->configFactory->get('lmstudio.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigPrefix(): string {
    return 'api_defaults.';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigSuffix(): string {
    return '.configuration';
  }

  /**
   * {@inheritdoc}
   */
  public function getModelSettings(): array {
    return ModelConfigLoader::loadSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function getProviderName(): string {
    return 'LM Studio';
  }

  /**
   * {@inheritdoc}
   */
  public function generateResponse(string $model_id, mixed $input, array $authentication = [], array $configuration = [], bool $normalise_io = TRUE): mixed {
    [$type] = explode('-', $model_id, 2);
    $configuration = array_merge($this->getDefaultConfigurationValues($model_id), $configuration);
    $endpoint = $this->modelsSettings[$type]['endpoint'];
    $request_data = [
      'messages' => $input,
    ] + $configuration;

    try {
      $response = $this->sendRequest($request_data, $endpoint);
      if ($normalise_io) {
        if ($type == StringUtility::pascalToSnake(Bundles::Chat->name)) {
          return $response['choices'][0]['message']['content'] ? trim($response['choices'][0]['message']['content']) : 'No response content found.';
        }
        else {
          return $response['data']['embedding'] ?: [];
        }
      }
      return $response;
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('lmstudio')->error($e->getMessage());
      return 'An error occurred while processing your request.';
    }
  }

  /**
   * Sends a request to the LM Studio local server.
   *
   * @param array $request_data
   *   The request data to send.
   * @param string $endpoint
   *   The URL.
   *
   * @return array
   *   The response data from the LM Studio local server.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function sendRequest(array $request_data, string $endpoint): array {
    $port = $this->config->get('port');
    $baseUrl = ($port) ? "{$this->config->get('domain')}:{$port}" : $this->config->get('domain');
    $response = $this->httpClient->post(
      $baseUrl . $endpoint,
      [
        'json' => $request_data,
        'timeout' => 120,
        'connect_timeout' => 120,
      ]
    );

    return json_decode($response->getBody()->getContents(), TRUE);
  }

}
